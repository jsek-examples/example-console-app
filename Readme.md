# Example console app

Simple console app boilerplate with pkg (generating *.exe) and fancy call stack.

![](screenshots/example.png)

---

## Build & Run

```shell
> npm i
> npm run pkg
> bin\pkg-test.exe
```

## Next steps

- Choose a framework
  - [Caporal.js](https://github.com/mattallty/Caporal.js)
  - or [Commander.js](https://tj.github.io/commander.js/)
  - or [command-line-args](https://github.com/75lb/command-line-args) and [command-line-usage](https://github.com/75lb/command-line-usage)
- Choose formatting utils
  - [hr](https://github.com/jaredsohn/hr)
  - [chalk](https://github.com/chalk/chalk)
  - [blessed](https://github.com/chjj/blessed)
  - [cli-table2](https://github.com/jamestalmage/cli-table2)
  - and [more](https://github.com/sindresorhus/awesome-nodejs#command-line-utilities)